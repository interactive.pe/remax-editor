import Vue from 'vue'
import svg from '~/utiles/svg'

Vue.mixin({
	methods: {
		...svg
	}
})
