export default {
	// Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
	ssr: false,
	server: {
		port: 80,
		host: '0.0.0.0',
	},

	// Target (https://go.nuxtjs.dev/config-target)
	target: 'static',

	// Global page headers (https://go.nuxtjs.dev/config-head)
	head: {
		title: 'Editor',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0' },
			{ name: 'HandheldFriendly', content: 'True' },
			{ hid: 'description', name: 'description', content: '' },
		],
		link: [{ rel: 'icon', type: 'image/png', href: '/icon.png' }],
	},

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css: [
		// '~/assets/css/tailwind.css'
		'~/assets/fuentes/stylesheet.css'
	],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins: [
		{ src: '~/plugins/utiles' },
		"~/plugins/select",
		{ src: '~/plugins/tabler.js' }
	],

	// Auto import components (https://go.nuxtjs.dev/config-components)
	components: true,

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules: [
		// https://go.nuxtjs.dev/stylelint
		// '@nuxtjs/stylelint-module',
		// https://go.nuxtjs.dev/tailwindcss
		'@nuxtjs/tailwindcss',
		// '@nuxtjs/google-fonts',
		'@nuxtjs/router-extras',
	],

	// Modules (https://go.nuxtjs.dev/config-modules)
	modules: [
		// https://go.nuxtjs.dev/axios
		'@nuxtjs/axios',
		// https://go.nuxtjs.dev/pwa
		'@nuxtjs/pwa',
		// https://go.nuxtjs.dev/content
		'@nuxt/content',
		'vue-sweetalert2/nuxt',
	],

	// Axios module configuration (https://go.nuxtjs.dev/config-axios)
	axios: {
		baseURL:"https://remaxdesignperu.com/system/patrones"
	},

	// Content module configuration (https://go.nuxtjs.dev/content-config)
	content: {},

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		postcss: {},
		extractCSS: true,
	},
	vue: {

	},
	router: {
		// base: "/",
		// base: "/remax-editor",
		base: "/editor",
		routes: [
			{
				path: '/',
				component: 'pages/index.vue',
				name: 'index'
			},
			// {
			// 	path: '/:id',
			// 	component: 'pages/index.vue',
			// 	name: 'index'
			// },
			{
				path: '/usuario/:id',
				component: 'pages/usuario.vue',
				name: 'usuario'
			},
			{
				path: '/creator/:id',
				component: 'pages/creator.vue',
				name: 'creator'
			},
		]
	},
	loading: {
		color: 'blue',
		height: '5px'
	},
	// googleFonts: {
	// 	families: {
	// 		Roboto: [400,700],
	// 		"Open+Sans": [400,700],
	// 		Raleway: [400,700],
	// 		Lato: [400,700],
	// 	}
	// }
}
