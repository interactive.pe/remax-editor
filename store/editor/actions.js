import _ from 'lodash'
import Vue from 'vue'
import { toPng, toJpeg, toBlob, toPixelData, toSvg } from 'html-to-image'
import JsPDF from "jspdf"
import axios from 'axios'

import loadImage from "blueimp-load-image"

function sleep(milliseconds) {
	const date = Date.now();
	let currentDate = null;
	do {
		currentDate = Date.now();
	} while (currentDate - date < milliseconds);
}

export default {
	// { commit, dispatch, getters, rootGetters, rootState, state }
	seleccionarElemento({commit, dispatch, state}, elemento){
		// console.debug(elemento)
		commit("actualizarElemento", elemento)

		const area = elemento.getBoundingClientRect()
		commit("actualizarSeleccion", {
			x: elemento.offsetLeft,
			y: elemento.offsetTop,
			width: area.width / state.zoom,
			height: area.height / state.zoom,
		})
		dispatch('contenido/seleccionarElementoPorId', elemento.id, {root:true})
	},
	moverElemento({commit, dispatch, state}, punto){
		// console.debug(punto)

		commit("contenido/modificarElemento", punto, {root:true})
		const elemento = state.elemento
		const area = elemento.getBoundingClientRect()
		commit("actualizarSeleccion", {
			x: elemento.offsetLeft,
			y: elemento.offsetTop,
			width: area.width / state.zoom,
			height: area.height / state.zoom,
		})
		dispatch("actualizarPrevio")
	},
	actualizarPagina({commit, dispatch, state}, valores){
		commit("contenido/actualizarPagina", valores, {root:true})
		dispatch("actualizarPrevio")
	},
	actualizarPropiedades({commit, dispatch, state}, valores){
		commit("contenido/actualizarPropiedades", valores, {root:true})
		dispatch("actualizarPrevio")
	},
	actualizarPermisos({commit, dispatch, state}, valores){
		commit("contenido/actualizarPermisos", valores, {root:true})
		dispatch("actualizarPrevio")
	},
	actualizarEstiloElemento({commit, dispatch, state}, valores){
		// console.debug(punto)

		commit("contenido/actualizarEstilo", valores, {root:true})

		_.delay( () => {
			const elemento = state.elemento
			const area = elemento.getBoundingClientRect()
			commit("actualizarSeleccion", {
				x: elemento.offsetLeft,
				y: elemento.offsetTop,
				width: area.width / state.zoom,
				height: area.height / state.zoom,
			})

			dispatch("actualizarPrevio")
		}, 1)
	},
	redimensionarElemento({commit, dispatch, state}, rectangulo){
		// console.debug(rectangulo)
		commit("contenido/modificarElemento", rectangulo, {root:true})
		const elemento = state.elemento
		const area = elemento.getBoundingClientRect()
		commit("actualizarSeleccion", {
			x: elemento.offsetLeft,
			y: elemento.offsetTop,
			width: area.width / state.zoom,
			height: area.height / state.zoom,
		})

		// dispatch("editor/actualizarPrevio")
		dispatch("actualizarPrevio")
	},
	actualizarAtributosElemento({commit, dispatch, state}, valores){
		commit("contenido/actualizarAtributos", valores, {root:true})
		dispatch("actualizarPrevio")
	},
	crearImagen({commit, dispatch, state}, ruta){

		const id = Math.random().toString(36).substring(7).toUpperCase()
		// const estilo = {
		// 	left: "0px",
		// 	top: "0px",
		// 	width: "320px",
		// 	height: "320px",
		// 	backgroundColor: "transparent"
		// }

		const atributos = {
			src: ruta
		}

		const nuevo =  {
			...state.plantillas.imagen,
			id,
			atributos
		}

		dispatch("contenido/crearElemento",nuevo, {root:true})

		// dispatch("contenido/crearElemento", {
		// 	id,
		// 	tag: "img",
		// 	atributos,
		// 	estilo,

		// 	permisos:{
		// 		movible: false,
		// 		redimensionable: false,
		// 		editable: true,
		// 	}
		// }, {root:true})
		dispatch("actualizarPrevio")
	},
	crearTexto({commit, dispatch, state}, texto){

		const id = Math.random().toString(36).substring(7).toUpperCase()
		const estilo = {
			left: "0px",
			top: "0px",
			width: "100px",
			height: "24px",
			backgroundColor: "transparent"
		}
		commit("anularSeleccion")

		// const elemento = Object.assign({
		// 	...require("./templates/texto.json"),
		// 	permisos: require("./templates/permisos.json")
		// }, {id, texto})
		// console.debug(estilo)
		// console.debug(elemento)

		// dispatch("contenido/crearElemento",
		// 	elemento,
		// 	{root:true})

		const nuevo = {
			...state.plantillas.texto,
			id,
			texto
		}

		dispatch("contenido/crearElemento", nuevo, {root:true})

		// dispatch("contenido/seleccionarElementoPorId", id, {root:true})
		_.delay( () => {
			dispatch("contenido/seleccionarElementoPorId", id, {root:true})
			dispatch("actualizarPrevio")
			// const elemento = state.elemento

			// const area = elemento.getBoundingClientRect()
			// commit("actualizarSeleccion", {
			// 	x: elemento.offsetLeft,
			// 	y: elemento.offsetTop,
			// 	width: area.width,
			// 	height: area.height,
			// })
		}, 1)
	},
	crearRectangulo({commit, dispatch, state}, color){

		const id = Math.random().toString(36).substring(7).toUpperCase()
		// const estilo = {
		// 	left: "0px",
		// 	top: "0px",
		// 	width: "100px",
		// 	height: "100px",
		// 	backgroundColor: color,
		// }
		commit("anularSeleccion")

		// dispatch("contenido/crearElemento", {
		// 	id,
		// 	tag: "div",
		// 	estilo,
		// 	permisos:{
		// 		movible: false,
		// 		redimensionable: false,
		// 		editable: true,
		// 	}
		// }, {root:true})

		const nuevo = {
			...state.plantillas.rectangulo,
			id
		}

		nuevo.estilo.backgroundColor = color

		dispatch("contenido/crearElemento", nuevo, {root:true})

		// dispatch("contenido/seleccionarElementoPorId", id, {root:true})
		_.delay( () => {
			dispatch("contenido/seleccionarElementoPorId", id, {root:true})
			// const elemento = state.elemento

			// const area = elemento.getBoundingClientRect()
			// commit("actualizarSeleccion", {
			// 	x: elemento.offsetLeft,
			// 	y: elemento.offsetTop,
			// 	width: area.width,
			// 	height: area.height,
			// })
			dispatch("actualizarPrevio")
		}, 1)
	},
	eliminarElemento({commit, dispatch, state}, texto){
		console.log("eliminarElemento")
		const elemento = state.elemento

		// elemento.parentElement.removeChild(elemento)

		dispatch("contenido/eliminarElemento", null, {root:true})
		commit("anularSeleccion")
		dispatch("actualizarPrevio")
		// _.delay( () => {
		// 	console.log("anularSeleccion")
		// 	commit("anularSeleccion")
		// }, 1)
	},
	async guardarPlantilla({commit, dispatch, state, rootState}){
		await dispatch("actualizarPrevio")
		sleep(500)

		const metodo = "/guardar"

		if(rootState.contenido.nombre){
			this.$axios.$post(metodo, {
				contenido: rootState.contenido.items,
				id: rootState.contenido.id,
				nombre: rootState.contenido.nombre
			})
			.then(response => {
					// this.actualizarID(parseInt(response.id))
					console.log(response)
					commit("contenido/actualizarID", parseInt(response.id), {root:true})
					commit("contenido/establecerModificado", false, {root:true})
					// alert("Guardado con éxito")

					Vue.swal({
						icon: 'success',
						title: 'Guardado',
						text: 'Guardado con éxito',
					})
			})
			.catch(function (error) {
				console.log(error);
			});

		}else{
			Vue.swal('Debe insertar un nombre');
		}

	},
	async guardarContenido({commit, dispatch, state, rootState}){
		await dispatch("actualizarPrevio")

		const metodo = "/publicacion/" + (rootState.contenido.id || "")

		if(rootState.contenido.nombre){
			this.$axios.$post(metodo, {
				contenido: rootState.contenido.items,
				id: rootState.contenido.id,
				cliente: state.usuario,
				nombre: rootState.contenido.nombre
			})
			.then(response => {
					// this.actualizarID(parseInt(response.id))
					console.log(response)
					commit("contenido/actualizarID", parseInt(response.id), {root:true})
					commit("contenido/establecerModificado", false, {root:true})
					// alert("Guardado con éxito")
					// console.log(rootState.contenido.id)

					Vue.swal({
						icon: 'success',
						title: 'Guardado',
						text: 'Guardado con éxito',
					})
			})
			.catch(function (error) {
				console.log(error);
			});

		}else{
			Vue.swal('Debe insertar un nombre');
		}

	},

	actualizarPrevio: _.debounce(async function({dispatch}) {
		dispatch("actualizaPrevio");
	}, 150),
	async actualizaPrevio({commit, dispatch, state, rootGetters}){
		// console.log(">> actualizaPrevio")

		if (state.contenedor) {
			const actual = rootGetters["contenido/actual"]
			const opciones = {
				width: actual.ancho,
				height: actual.alto,
				pixelRatio: 1,
				cacheBust: true,
				quality: 0.85,

				style:{
					transform: "scale(1)"
				}
			}


			//console.debug("actualizarPrevio", rootGetters["contenido/actual"])
			// console.debug(opciones)

			// toJpeg(state.contenedor, opciones)
			// 	.then(function (dataUrl) {
			// 		// console.debug(dataUrl)
			// 		commit("contenido/actualizarPrevio", dataUrl, {root:true})
			// 	});

			const previo = await toJpeg(state.contenedor, opciones);
			console.log("previo", previo)
			// console.log("previo")
			const {image} = await loadImage(previo, {
				maxWidth: 640,
				maxHeight: 640,
				canvas: true
			})

			const dataURL = image.toDataURL()
			commit("contenido/actualizarPrevio", dataURL, {root:true})

		}
	},
	async cargarContenido({commit, dispatch, state, rootState}, id){
		const datos = await this.$axios.$get("/publicacion/" + id)

		commit("contenido/establecerId", id, {root:true})
		commit("contenido/establecerNombre", datos.nombre, {root:true})
		commit("contenido/cargar", JSON.parse(datos.contenido), {root:true})
	},
	async cargarPlantilla({commit, dispatch, state, rootState}, id){
		const datos = await this.$axios.$get("/abrir/" + id)

		console.log(datos)

		commit("contenido/establecerId", id, {root:true})
		commit("contenido/establecerNombre", datos.nombre, {root:true})
		commit("contenido/cargar", JSON.parse(datos.contenido), {root:true})
	},

	async descargarImagen({commit, dispatch, state, rootState, rootGetters}){

		const actual = rootGetters["contenido/actual"]
		const opciones = {
			width: actual.ancho,
			height: actual.alto,
			pixelRatio: 1,
			cacheBust: true,
			quality: 1
		}
		// console.debug("state.nombre", rootState.contenido.nombre)
		// console.debug(opciones)

		// sleep(2000)
		if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
			await toJpeg(state.contenedor, opciones)
		}

		toJpeg(state.contenedor, opciones)
			.then(function (dataUrl) {
				var link = document.createElement('a')
				link.download = `${rootState.contenido.nombre}.jpg`
				link.href = dataUrl
				link.click()
			});
	},


	async descargarPDF({commit, dispatch, state, rootGetters}){


		const actual = rootGetters["contenido/actual"]
		const DPI = 150
		const docAncho = (actual.ancho * 25.4) / DPI
		const docAlto = (actual.alto * 25.4) / DPI


		const pdf = new JsPDF({
			orientation: "l",
			unit: "mm",
			format: [docAncho, docAlto],
			compress: true
		})

		const opciones = {
			width: actual.ancho,
			height: actual.alto,
			pixelRatio: 1,
			quality: 1,
			cacheBust: true
		}

		// state.contenedor.style.transform= "scale(2)"

		if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
			await toPng(state.contenedor, opciones)
		}
		const imagen = await toPng(state.contenedor, opciones)
		// mm = ( pixels * 25.4 ) / DPI

		pdf.addImage(
			imagen,
			"PNG",
			0,
			0,
			docAncho,
			docAlto,
		)

		pdf.save("doc.pdf")
	}
}
