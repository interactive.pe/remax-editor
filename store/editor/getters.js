export default {
	// { state, getters, rootState, rootGetters }
	ancho: (state, getters, rootState, rootGetters) => {
		const margen = rootState.opciones.margen
		const pagina = rootGetters["contenido/actual"]

		return pagina.ancho + ( margen * 2 )
	},
	alto: (state, getters, rootState, rootGetters) => {
		const margen = rootState.opciones.margen
		const pagina = rootGetters["contenido/actual"]

		return pagina.alto + ( margen * 2 )
	},
	anchoTotal: (state, getters, rootState, rootGetters) => {
		const margen = rootState.opciones.margen
		const pagina = rootGetters["contenido/actual"]

		return pagina.ancho + ( margen * 2 )
	},
	altoTotal: (state, getters, rootState, rootGetters) => {
		const margen = rootState.opciones.margen
		const pagina = rootGetters["contenido/actual"]

		return pagina.alto + ( margen * 2 )
	},
	// seleccion: state => {
	// 	if (state.elemento) {
	// 		const area = state.elemento.getBoundingClientRect()
	// 		return {
	// 			x: state.elemento.offsetLeft,
	// 			y: state.elemento.offsetTop,
	// 			width: area.width,
	// 			height: area.height,
	// 		}
	// 	}
	// 	else{
	// 		return null
	// 	}


	// }


}
