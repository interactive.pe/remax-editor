export default () => ({
	seleccion: null,
	modo: "usuario",
	usuario: 0,
	id: null,
	zoom: 1,
	contenedor: null,
	elemento: null,
	plantillas: {
		texto: {
			...require("./templates/texto.json"),
			permisos: require("./templates/permisos.json")
		},
		imagen:{
			...require("./templates/imagen.json"),
			permisos: require("./templates/permisos.json")

		},
		rectangulo:{
			...require("./templates/rectangulo.json"),
			permisos: require("./templates/permisos.json")
		}
	},
	permisos: require("./templates/permisos.json"),
	fuentes: [
		"sans-serif",
		"Roboto",
		"Open Sans",
		"Raleway",
		"Lato",
		"Montserrat"
	],
	"1px": "image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="
})
