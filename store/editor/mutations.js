export default {
	moverSeleccion(state, {x, y}) {
		// console.log(state)
		state.seleccion.x = x
		state.seleccion.y = y
	},
	modificarSeleccion(state, {width, height}) {
		// console.log(state)
		state.seleccion.width = width || state.seleccion.width
		state.seleccion.height = height || state.seleccion.height
	},
	actualizarSeleccion(state, rectangulo) {
		// console.log(state)
		// console.log("actualizarSeleccion >", rectangulo)
		state.seleccion = rectangulo
	},
	anularSeleccion(state) {
		console.log("anularSeleccion >")
		state.elemento = null
		state.seleccion = null
		console.log("anularSeleccion >", state.seleccion)
	},

	actualizarElemento(state, elemento) {
		state.elemento = elemento
	},

	actualizarUsuario(state, usuario) {
		state.usuario = usuario
	},

	establecerContenedor(state, nodo) {
		console.log("contenedorActual", nodo)
		state.contenedor = nodo
	},
	moverElemento(state, {x, y}) {
		// console.log(state)

	},

	establecerModo(state, modo) {
		state.modo = modo
	},

	actualizarZoom(state, valor) {
		state.zoom = valor
	},
}
