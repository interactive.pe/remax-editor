export default {
	// { commit, dispatch, getters, rootGetters, rootState, state }
	seleccionarElementoPorId({commit, getters}, id){
		console.debug( "seleccionarElementoPorId", id, getters.objetos.findIndex(item => item.id == id))
		commit("activo", getters.objetos.findIndex(item => item.id == id))
	},
	crearElemento({commit, getters}, objeto){
		commit("crearElemento", objeto)
	},
	eliminarElemento({commit, getters}, objeto){
		commit("eliminarElemento")
	},
}
