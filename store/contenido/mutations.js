export default {
	actual(state, id) {
		state.actual = id

		state.modificado = true
	},
	activo(state, id) {
		state.activo = id

		state.modificado = true
	},
	cargar(state, contenido) {
		// const objeto = state.paginas[state.actual].objetos.

		state.items = contenido

		state.modificado = true
	},
	actualizarID(state, id) {
		// const objeto = state.paginas[state.actual].objetos.

		state.id = id

		// state.modificado = true
	},

	actualizar(state, { pagina, indice, datos }) {
		// const objeto = state.paginas[state.actual].objetos.

		state.items[pagina].objetos[indice] = datos

		state.modificado = true
	},
	// moverElemento(state, { x, y }) {
	// 	// const objeto = state.paginas[state.actual].objetos.

	// 	const objeto = state
	// 		.items[state.actual]
	// 			.objetos[state.activo]

	// 	Object.assign(objeto.estilo, {
	// 		left: `${x}px`,
	// 		top: `${y}px`,
	// 	})
	// },
	actualizarPagina(state, atributos) {
		// const objeto = state.paginas[state.actual].objetos.

		const paginas = state.items
		const pagina = state.items[state.actual]
		console.log(pagina)
		// paginas[state.actual] = Object.assign(pagina, atributos)
		// state.items = paginas

		state.items[state.actual] = Object.assign(pagina, atributos)

		state.modificado = true

	},
	// moverElemento(state, { x, y }) {
	// 	// const objeto = state.paginas[state.actual].objetos.

	// 	const objeto = state
	// 		.items[state.actual]
	// 			.objetos[state.activo]

	// 	Object.assign(objeto.estilo, {
	// 		left: `${x}px`,
	// 		top: `${y}px`,
	// 	})
	// },
	modificarElemento(state, { x, y, ancho, alto }) {
		// const objeto = state.paginas[state.actual].objetos.
		// console.debug("modificarElemento", ancho, alto)
		const objeto = state
			.items[state.actual]
				.objetos[state.activo]

		if(objeto){
			const estilo = {...objeto.estilo}

			objeto.estilo = Object.assign(estilo, {
				left: x ? `${x}px` : estilo.left,
				top: y ? `${y}px` : estilo.top,
				width: ancho ? `${ancho}px` : estilo.width,
				height: alto ? `${alto}px` : estilo.height,
			})

			state.modificado = true

		}
	},
	actualizarEstilo(state, valores) {
		// const objeto = state.paginas[state.actual].objetos.
		// console.debug("modificarElemento", ancho, alto)
		const objeto = state
			.items[state.actual]
				.objetos[state.activo]

		const estilo = {...objeto.estilo}

		objeto.estilo = Object.assign(estilo, valores)

		state.modificado = true
	},

	actualizarAtributos(state, valores) {
		// const objeto = state.paginas[state.actual].objetos.

		const objeto = state
		.items[state.actual]
			.objetos[state.activo]

		const atributos = {...objeto.atributos}

		objeto.atributos = Object.assign(atributos, valores)

		state.modificado = true
	},


	actualizarPermisos(state, permisos) {
		// const objeto = state.paginas[state.actual].objetos.

		const objeto = state
		.items[state.actual]
			.objetos[state.activo]

		// const permisos = {}

		objeto.permisos = Object.assign(objeto.permisos, permisos)

		state.modificado = true
	},

	actualizarPropiedades(state, valores) {
		// const objeto = state.paginas[state.actual].objetos.

		const objeto = state
			.items[state.actual]
				.objetos[state.activo]

		state
			.items[state.actual]
				.objetos[state.activo] = Object.assign(objeto, valores)

		state.modificado = true
	},

	crearElemento(state, objeto) {
		// const objeto = state.paginas[state.actual].objetos.
		console.debug("crearElemento", objeto)


		const objetos = state
			.items[state.actual]
				.objetos.push(objeto)

		state.modificado = true

		// state
		// 	.items[state.actual]
		// 		.objetos = objetos

		//state.activo = -1
	},
	eliminarElemento(state) {
		// const objeto = state.paginas[state.actual].objetos.
		console.log("eliminarElemento")


		const objetos = state
			.items[state.actual]
				.objetos.splice(state.activo, 1)

		// state
		// 	.items[state.actual]
		// 		.objetos = objetos

		state.activo = -1
		state.modificado = true
	},
	subirElemento(state) {

		const objetos = state
			.items[state.actual]
				.objetos

		const objeto = state
			.items[state.actual]
				.objetos[state.activo]

		objetos.splice(state.activo, 1)
		objetos.push(objeto)
		state.modificado = true
	},
	bajarElemento(state) {

		const objetos = state
			.items[state.actual]
				.objetos

		const objeto = state
			.items[state.actual]
				.objetos[state.activo]

		objetos.splice(state.activo, 1)
		objetos.unshift(objeto)
		state.modificado = true
	},

	actualizarPrevio(state, imagen){

		state
			.items[state.actual].previo = imagen
			// console.log("actualizarPrevio", state
				// .items[state.actual].previo)
	},

	establecerId(state, valor){
		state.id = valor
	},
	establecerNombre(state, valor){
		state.nombre = valor
	},

	establecerModificado(state, valor){
		state.modificado = valor
	},

	nuevoItem(state){
		state.items.push({
			ancho: 500,
			alto: 500,
			fondo: "#FFFFFF",
			objetos: []
		})

		state.actual++
	},

	borrarItem(state, id){
		state.items.splice(id, 1)
		// state.actual = Math.min(state.items.length, Math.Max(id - 1, 0))
		state.actual = Math.max(Math.min(state.items.length - 1, state.actual), 0)
	}
}
