export default {
	actual: state => state.items[state.actual] || {objetos:[]},
	objetos: (state, getters) => getters.actual.objetos,
	activo: (state, getters) => getters.objetos[state.activo],
	permisos: (state, getters) => getters.activo.permisos
}
